﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongArithmetic
{
    class DSS
    {
        static private bool isPrime(BigInt p)
        {
            BigInt ZERO = new BigInt("0");
            BigInt ONE = new BigInt("1");
            BigInt TWO = new BigInt("2");

            BigInt p1 = p - ONE;

            int s = 0;

            BigInt t = new BigInt(p1);
            while (t % TWO == ZERO)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 20; i++)
            {
                Random rand = new Random();
                BigInt tmp = new BigInt("0");
                while (tmp > p1 || tmp < TWO)
                {
                    string str = "";
                    for (int j = 0; j < p1.GetLength(); j++)
                        str += rand.Next() % p1.GetBase();

                    tmp = new BigInt(str);
                    tmp %= p;
                }

                BigInt x = BigInt.ModPow(tmp, t, p);

                if (x <= ONE || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInt.ModPow(x, TWO, p);

                    if (x == ONE)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }

        static public BigInt getG(ref BigInt p, BigInt q, ref BigInt s)
        {
            BigInt ZERO = new BigInt("0");
            BigInt ONE = new BigInt("1");
            BigInt TWO = new BigInt("2");

            BigInt g = new BigInt("1");

            if (!isPrime(q))
            {
                throw new ArgumentException("Введено не простое число");
            }

            Random rand = new Random();
            s = new BigInt("0");
            string str = "";
            for (int k = 0; k < q.GetLength(); k++)
                str += rand.Next() % q.GetBase();

            s = new BigInt(str);
            s %= q;

            if (s < TWO) { s = new BigInt(TWO); }
            if (s > TWO && s % TWO != ZERO) { s -= ONE; }

            BigInt _s = new BigInt(s);
            do
            {
                p = new BigInt(q);
                p *= s;
                p += ONE;

                _s = new BigInt(s);
                s += TWO;

                s %= q;

                if (s < TWO) { s = new BigInt(TWO); }
            } while (!isPrime(p));

            s = _s;

            BigInt p1 = p - ONE;
            BigInt p2 = p - TWO;

            BigInt t = p1 / q;

            while (g == ONE)
            {
                BigInt a = new BigInt("0");
                while (a > p2 || a < TWO)
                {
                    str = "";
                    for (int k = 0; k < p1.GetLength(); k++)
                        str += rand.Next() % p1.GetBase();

                    a = new BigInt(str);
                }
                
                g = BigInt.ModPow(a, t, p);                    
            }

            return g;
        }
    }
}
