﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongArithmetic
{
    class Elgamal
    {
        private BigInt ZERO = new BigInt("0");
        private BigInt ONE = new BigInt("1");
        private BigInt TWO = new BigInt("2");

        private string Dictionary = "abcdefghijklmnopqrstuvwxyz" +
                                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                                    "абвгдеёжзийклмнопрстуфхцчшщьыъэюя" +
                                    "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ" +
                                    "0123456789" +
                                    "!@#$%^&*()_+-=\\|/:;'\"<>[]{},.?~` ";

        private BigInt p;
        private BigInt g = new BigInt("1");
        private BigInt y;
        private BigInt x = new BigInt("1");

        private int n;

        public Elgamal(int n = 30)
        {
            this.n = n;

            BigInt q = genPrime(this.n);
            BigInt s = new BigInt("0");
            this.g = DSS.getG(ref this.p, q, ref s);

            Random rand = new Random();
            this.x = new BigInt("0");
            string str = "";
            for (int i = 0; i < p.GetLength(); i++)
            {
                str += rand.Next() % p.GetBase();
            }
            this.x = new BigInt(str);

            this.x = (this.x % (this.p - TWO)) + ONE;

            this.y = BigInt.ModPow(g, x, p);

            int j = 0;
            while (j < Dictionary.Length)
            {
                string ds = j.ToString("000");
                for (int k = 0; k < ds.Length; k++)
                {
                    if (ds[k] == '0')
                    {
                        Dictionary = Dictionary.Insert(j, "`");
                        break;
                    }
                }
                j++;
            }
        }

        public Elgamal(BigInt p, BigInt g, BigInt y)
        {
            if (!isPrime(p))
            {
                throw new ArgumentException("Введено не простое число p");
            }

            this.p = p;
            this.g = g;
            this.y = y;

            this.n = p.GetLength();

            int j = 0;
            while (j < Dictionary.Length)
            {
                string ds = j.ToString("000");
                for (int k = 0; k < ds.Length; k++)
                {
                    if (ds[k] == '0')
                    {
                        Dictionary = Dictionary.Insert(j, "`");
                        break;
                    }
                }
                j++;
            }
        }

        public Elgamal(BigInt p, BigInt x)
        {
            if (!isPrime(p))
            {
                throw new ArgumentException("Введено не простое число p");
            }

            this.p = p;
            this.x = x;
            this.y = BigInt.ModPow(g, x, p);

            this.n = p.GetLength();

            int j = 0;
            while (j < Dictionary.Length)
            {
                string ds = j.ToString("000");
                for (int k = 0; k < ds.Length; k++)
                {
                    if (ds[k] == '0')
                    {
                        Dictionary = Dictionary.Insert(j, "`");
                        break;
                    }
                }
                j++;
            }
        }

        public Elgamal(BigInt q)
        {
            if (!isPrime(q))
            {
                throw new ArgumentException("Введено не простое число q");
            }

            BigInt s = new BigInt("0");
            this.g = DSS.getG(ref this.p, q, ref s);

            Random rand = new Random();
            this.x = new BigInt("0");
            string str = "";
            for (int i = 0; i < p.GetLength(); i++)
            {
                str += rand.Next() % p.GetBase();
            }
            this.x = new BigInt(str);

            this.x = (this.x % (this.p - TWO)) + ONE;

            this.y = BigInt.ModPow(g, x, p);

            this.n = p.GetLength();

            int j = 0;
            while (j < Dictionary.Length)
            {
                string ds = j.ToString("000");
                for (int k = 0; k < ds.Length; k++)
                {
                    if (ds[k] == '0')
                    {
                        Dictionary = Dictionary.Insert(j, "`");
                        break;
                    }
                }
                j++;
            }
        }

        public Dictionary<char, BigInt> getPublicKey()
        {
            Dictionary<char, BigInt> ret = new Dictionary<char, BigInt>();

            ret.Add('p', new BigInt(p));
            ret.Add('g', new BigInt(g));
            ret.Add('y', new BigInt(y));

            return ret;
        }

        public BigInt getPrivateKey()
        {
            if (x == ONE)
            {
                throw new Exception("Был проинициализирован вариант только для шифрования");
            }

            return new BigInt(x);
        }

        private bool isPrime(BigInt p)
        {
            BigInt p1 = p - ONE;

            int s = 0;

            BigInt t = new BigInt(p1);
            while (t % TWO == ZERO)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 20; i++)
            {
                Random rand = new Random();
                BigInt tmp = new BigInt("0");
                while (tmp > p1 || tmp < TWO)
                {
                    string str = "";
                    for (int j = 0; j < p1.GetLength(); j++)
                        str += rand.Next() % p1.GetBase();

                    tmp = new BigInt(str);
                    tmp %= p;
                }

                BigInt x = BigInt.ModPow(tmp, t, p);

                if (x <= ONE || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInt.ModPow(x, TWO, p);

                    if (x == ONE)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }

        private BigInt genPrime(int n)
        {
            Random rand = new Random();

            BigInt p = new BigInt("0");
            string str = ((rand.Next() % 9) + 1) + "";
            for (int i = 1; i < n; i++)
            {
                str += rand.Next() % p.GetBase();
            }
            p = new BigInt(str);

            if (p % TWO == ZERO) { p -= ONE; }

            while (!isPrime(p))
            {
                p += TWO;

                if (p.GetLength() > n)
                {
                    str = "1";
                    for (int i = 2; i < n; i++)
                    {
                        str += "0";
                    }
                    str += "1";
                    p = new BigInt(str);
                }
            }

            return p;
        }

        public string Encrypt(string massage)
        {
            if (g == ONE)
            {
                throw new Exception("Был проинициализирован вариант только для дешифрования");
            }

            string numMassage = "";
            for (int j = 0; j < massage.Length; j++)
            {
                numMassage += (Dictionary.IndexOf(massage[j])).ToString("000");
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Сообщение, представленное в числовом виде:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(numMassage);
            Console.WriteLine();

            string encodedText = "";

            Random rand = new Random();
            for (int i = 0; i < numMassage.Length;)
            {
                string strM = "0";
                BigInt m = new BigInt(strM);

                BigInt k = new BigInt("0");

                string str = "";
                while (i < numMassage.Length && new BigInt(strM + numMassage[i]) < p)
                {
                    strM += numMassage[i];
                    m = new BigInt(strM);
                    i++;
                }

                
                str = "";
                for (int j = 0; j < n; j++)
                {
                    str += rand.Next() % k.GetBase();
                }
                k = new BigInt(str);
                k =  (k % (p - TWO)) + ONE;

                BigInt a = BigInt.ModPow(g, k, p);
                BigInt b = (BigInt.ModPow(y, k, p) * m) % p;

                Console.WriteLine(" k = " + k + "\tm = " + m);

                encodedText += a + " " + b + " ";
            }
            Console.WriteLine();

            return encodedText;
        }

        public string Decrypt(string encodedText)
        {
            if (x == ONE)
            {
                throw new Exception("Был проинициализирован вариант только для шифрования");
            }

            string[] et = encodedText.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string massage = "";            

            string numMassage = "";

            for (int i = 1; i < et.Length; i += 2)
            {
                BigInt a = new BigInt(et[i - 1]);
                BigInt b = new BigInt(et[i]);

                BigInt m = (b * BigInt.ModPow(a, p - ONE - x, p)) % p;

                Console.WriteLine("m = " + m);

                string massagePart = m.ToString();

                for (int j = 0; j < massagePart.Length; j++)
                {
                    numMassage += massagePart[j];
                }
            }

            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Расшифрованное сообщение в числовом виде:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(numMassage);
            Console.WriteLine();

            for (int j = 3; j <= numMassage.Length; j += 3)
            {
                try
                {
                    massage += Dictionary[int.Parse(numMassage.Substring(j - 3, 3))];
                }
                catch { }
            }

            return massage;
        }
    }
}
