﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Numerics;

namespace LongArithmetic
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Введите текст: ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            string text = Console.ReadLine();
            Console.WriteLine();*/

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Введите q: ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            string str_q = Console.ReadLine();
            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Введите текст: ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            string text = Console.ReadLine();
            Console.WriteLine();

            Elgamal eg = new Elgamal(new BigInt(str_q));

            BigInt privateKey = eg.getPrivateKey();
            Dictionary<char, BigInt> publicKey = eg.getPublicKey();

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Публичный ключ: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(" p = " + publicKey['p']);
            Console.WriteLine(" g = " + publicKey['g']);
            Console.WriteLine(" y = " + publicKey['y']);
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Частный ключ: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(" x = " + privateKey);
            Console.WriteLine();

            string et = eg.Encrypt(text);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Зашифрованное сообщение: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(et);
            Console.WriteLine();

            string dt = eg.Decrypt(et);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Расшифрованное сообщение: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(dt);
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
        }
    }
}
